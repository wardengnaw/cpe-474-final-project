#include <iostream>
#include "Scene.h"
#include "Particle.h"
#include "Shape.h"
#include "Program.h"
#include "Bridge.h"

Scene::Scene() :
	t(0.0),
	h(1e-2),
	grav(0.0, 0.0, 0.0),
   bridge(NULL)
{
}

Scene::~Scene()
{
	while(!spheres.empty()) {
		delete spheres.back();
		spheres.pop_back();
	}

   delete bridge;

	delete sphereShape;
}

void Scene::load()
{
	// Units: meters, kilograms, seconds
	h = 1e-3;

   bridges[0] = new Bridge(1);
   bridges[1] = new Bridge(2);
   bridges[2] = new Bridge(3);

   curBridge = 0;
   bridge = bridges[curBridge];

	grav << 0.0, -9.8, 0.0;
   damp = 1e5;

   externalForce << 0.0, 0.0, 0.0;
   externPos << 0.0, 0.0, 0.0;
}

void Scene::switchBridge() {
  curBridge = (curBridge + 1) % 3;
  bridge = bridges[curBridge];
}

void Scene::init()
{
   bridges[0]->init();
   bridges[1]->init();
   bridges[2]->init();
}

void Scene::tare()
{
   bridges[0]->tare();
   bridges[1]->tare();
   bridges[2]->tare();
}

void Scene::reset()
{
   bridge->reset();
}

void Scene::step(bool breakingBridge)
{
	t += h;

   bridge->step(h, grav, damp, externalForce, externPos, breakingBridge);
}

void Scene::draw(MatrixStack &MV, const Program &prog) const
{
   bridge->draw(MV, prog);
}

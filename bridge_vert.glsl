#version 120

uniform mat4 P;
uniform mat4 MV;

attribute vec3 vertPos;
attribute vec3 vertColor;

varying vec3 color;

void main()
{
	gl_Position = P * MV * vec4(vertPos, 1);
	//color = gl_Color.rgb;
	color = vertColor;
}

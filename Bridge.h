#pragma once
#ifndef __BRIDGE_H__
#define __BRIDGE_H__
#include <stdio.h>
#include <vector>
#include <Eigen/Dense>

class Particle;
class MatrixStack;
class Program;
class Beam;

class Bridge
{
   public:
      Bridge(int selection);
      virtual ~Bridge();
	   void init();
      
      void tare();
      void reset();

      void update();

      void step(double h, const Eigen::Vector3d &grav, const double damp, const Eigen::Vector3d &externalForce, 
                const Eigen::Vector3d &externPos, bool breakingBridge);
	   void draw(MatrixStack &MV, const Program &p) const;

   private:
	   std::vector<Particle*> initParticles;
      std::vector<Beam*> initBeams;
      
      std::vector<Particle*> particles;
	   std::vector<Beam*> beams;
	   
      Eigen::MatrixXd M;
      Eigen::MatrixXd K;
      Eigen::VectorXd v;
      Eigen::VectorXd f;

      int oldN;
      int n;
	   
      unsigned posBufID;
      unsigned colorBufID;
      std::vector<float> posBuf;
      std::vector<float> colorBuf;
};
#endif 

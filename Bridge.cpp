#include <iostream>
#include <Eigen/Sparse>
#include "Bridge.h"
#include "Particle.h"
#include "Beam.h"
#include "MatrixStack.h"
#include "Program.h"
#include "GLSL.h"

using namespace std;


Beam* createBeam(Particle *p0, Particle *p1, double K, bool isRoad)
{
	Beam *b = new Beam(p0, p1);
	b->K = K;
	Eigen::Vector3d x0 = p0->x;
	Eigen::Vector3d x1 = p1->x;
	Eigen::Vector3d dx = x1 - x0;
	b->L = dx.norm();
	b->isRoad = isRoad;
   return b;
}

double getDistance(Eigen::Vector3d externPos, Particle *p) {
   Eigen::Vector3d tempP;
   tempP << p->x(0), 0.0, 0.0;
   
   Eigen::Vector3d pPos = tempP - externPos;

   return pPos.norm();
   
}

bool checkIfBetween(Eigen::Vector3d pos, Particle *p0, Particle *p1) {
   Eigen::Vector3d p0x = p0->x;
   Eigen::Vector3d p1x = p1->x;

   return (pos(0) >= p0x(0) && pos(0) <= p1x(0) || (pos(0) <= p0x(0) && pos(0) >= p1x(0)));
}

Bridge::Bridge(int selection) {
  n = 0;
  
  Particle *p;
  
  if (selection == 2) {
     for (int i = 0; i <= 10; i++) {
        p = new Particle();
        particles.push_back(p);
        p->x << i * 0.2 - 1.0, 0.0, 0.0;
        p->v << 0.0, 0.0, 0.0;
        p->m = 0.0;
        if (i == 0 || i == 10) {
           p->fixed = true;
           p->i = -1;
        }
        else {
           p->fixed = false;
           p->i = n;
           n+= 3;
        }
     }
     for (int i = 0; i < 10; i++) {
        beams.push_back(createBeam(particles[i], particles[i + 1], 1e9, true)); 
        particles[i]->m += 200;
        particles[i + 1]->m += 200;
     }

     for (int i = 0; i <= 9; i++) {
        p = new Particle();
        particles.push_back(p);
        p->x << i * 0.2 - 0.9, 0.5, 0.0;
        p->v << 0.0, 0.0, 0.0;
        p->m = 1.0;
        p->fixed = false;
        p->i = n;
        n+= 3;
     }
     
     for (int i = 0; i < 9; i++) {
        beams.push_back(createBeam(particles[i + 11], particles[i + 12], 1e9, false)); 
        particles[i + 11]->m += 200;
        particles[i + 12]->m += 200;
     }
     
     for (int i = 0; i < 10; i++) {
        beams.push_back(createBeam(particles[i], particles[i + 11], 1e7, false));
        beams.push_back(createBeam(particles[i + 1], particles[i + 11], 1e9, false));
        particles[i]->m += 200;
        particles[i + 1]->m += 200;
        particles[i + 11]->m += 200;
        particles[i + 11]->m += 200;
     }
  }
  else if (selection == 1) {
     for (int i = 0; i < 5; i++) {
        p = new Particle();
        particles.push_back(p);
        p->x << i * 0.4 - 0.8, 0.0, 0.0;
        p->v << 0.0, 0.0, 0.0;
        p->m = 0.0;
        if (i == 0 || i == 4) {
           p->fixed = true;
           p->i = -1;
        }
        else {
           p->fixed = false;
           p->i = n;
           n+= 3;
        }
     }
     
     for (int i = 0; i < 4; i++) {
        beams.push_back(createBeam(particles[i], particles[i + 1], 1e9, true)); 
        particles[i]->m += 200;
        particles[i + 1]->m += 200;
     }
  } else if (selection == 3) {
     for (int i = 0; i < 5; i++) {
        p = new Particle();
        particles.push_back(p);
        p->x << i * 0.5 - 1.0, 0.0, 0.0;
        p->v << 0.0, 0.0, 0.0;
        p->m = 0.0;
        if (i == 0 || i == 4 || i == 2) {
           p->fixed = true;
           p->i = -1;
        }
        else {
           p->fixed = false;
           p->i = n;
           n+= 3;
        }
     }
     
     for (int i = 0; i < 4; i++) {
        beams.push_back(createBeam(particles[i], particles[i + 1], 1e9, true)); 
        particles[i]->m += 200;
        particles[i + 1]->m += 200;
     }
  
     for (int i = 0; i < 4; i++) {
        p = new Particle();
        particles.push_back(p);
        p->x << i * 0.5 - 0.75, 0.5, 0.0;
        p->v << 0.0, 0.0, 0.0;
        p->m = 0.0;
        p->fixed = false;
        p->i = n;
        n+= 3;
     }
     
     for (int i = 0; i < 3; i++) {
        beams.push_back(createBeam(particles[i + 5], particles[i + 6], 1e9, false)); 
        particles[i + 5]->m += 200;
        particles[i + 6]->m += 200;
     }
     
     for (int i = 0; i < 4; i++) {
        beams.push_back(createBeam(particles[i], particles[i + 5], 1e7, false));
        beams.push_back(createBeam(particles[i + 1], particles[i + 5], 1e9, false));
        particles[i]->m += 200;
        particles[i + 1]->m += 200;
        particles[i + 5]->m += 200;
        particles[i + 5]->m += 200;
     }
  
  }

  posBuf.clear();
  posBuf.resize(beams.size() * 2 * 3);
  
  colorBuf.clear();
  colorBuf.resize(beams.size() * 2 * 3);

  for (int i = 0; i < particles.size(); i++) {
     initParticles.push_back(particles[i]);
  }

  for (int i = 0; i < beams.size(); i++) {
     initBeams.push_back(beams[i]);
  }

  oldN = n;

  update();
}

Bridge::~Bridge()
{
	while(!beams.empty()) {
		delete beams.back();
		beams.pop_back();
	}
	while(!particles.empty()) {
		delete particles.back();
		particles.pop_back();
	}
}

void Bridge::tare()
{
	for(int k = 0; k < (int)particles.size(); ++k) {
		particles[k]->tare();
	}
}

void Bridge::reset()
{
	particles.clear();
   beams.clear();

   for (int i = 0; i < initParticles.size(); i++) {
      particles.push_back(initParticles[i]);
   }
  
   for (int i = 0; i < initBeams.size(); i++) {
      beams.push_back(initBeams[i]);
   }

   n = oldN;

   for(int k = 0; k < (int)particles.size(); ++k) {
		particles[k]->reset();
	   //std::cout << particles[k]->x << std::endl;
   }
   update();
}

void Bridge::init()
{
	glGenBuffers(1, &posBufID);
	glBindBuffer(GL_ARRAY_BUFFER, posBufID);
	glBufferData(GL_ARRAY_BUFFER, posBuf.size()*sizeof(float), &posBuf[0], GL_DYNAMIC_DRAW);
	
   glGenBuffers(1, &colorBufID);
	glBindBuffer(GL_ARRAY_BUFFER, colorBufID);
	glBufferData(GL_ARRAY_BUFFER, colorBuf.size()*sizeof(float), &colorBuf[0], GL_DYNAMIC_DRAW);
	
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	assert(glGetError() == GL_NO_ERROR);
}

void Bridge::update() {
   posBuf.clear();
   colorBuf.clear();
   
   for (int i = 0; i < beams.size(); i++) {
      Eigen::Vector3d p0x = beams[i]->p0->x; 
      Eigen::Vector3d p1x = beams[i]->p1->x; 
      
      //std::cout << "Line from (" << p0x(0) << ", " << p0x(1) << ". " << p0x(2) << ") to (" << p1x(0) << ", " << p1x(1) << ", " << p1x(2) << ")" << std::endl;
      
      posBuf.push_back(p0x(0));
      posBuf.push_back(p0x(1));
      posBuf.push_back(p0x(2));
      posBuf.push_back(p1x(0));
      posBuf.push_back(p1x(1));
      posBuf.push_back(p1x(2)); 
      
      if (beams[i]->strain < 0) {
         colorBuf.push_back(-beams[i]->strain * 100);
         colorBuf.push_back(0.0);
      }
      else {
         colorBuf.push_back(0.0);
         colorBuf.push_back(beams[i]->strain * 100);
      }
      colorBuf.push_back(0.0);
      
      if (beams[i]->strain < 0) {
         colorBuf.push_back(-beams[i]->strain * 100);
         colorBuf.push_back(0.0);
      }
      else {
         colorBuf.push_back(0.0);
         colorBuf.push_back(beams[i]->strain * 100);
      }
      colorBuf.push_back(0.0); 
   }
}

void Bridge::step(double h, const Eigen::Vector3d &grav, const double damp, const Eigen::Vector3d &externalForce, const Eigen::Vector3d &externPos, bool breakingBridge) {
	M.setZero();
	K.setZero();
	v.setZero();
	f.setZero();
	Eigen::Matrix3d I = Eigen::Matrix3d::Identity();


	// IMPLEMENT ME
   int matIdx = 0;
  

   for (int i = 0; i < beams.size(); i++) {
         Eigen::Vector3d deltaX = beams[i]->p1->x - beams[i]->p0->x; 
         double l = deltaX.norm();
         double L = beams[i]->L;
         
         beams[i]->strain = (l - L) / L;

      if (breakingBridge) {
            if (beams[i]->strain > 0.02) {
               Particle *particle_temp = new Particle();
               if (rand() % 10 < 5) {
                  particle_temp->x << Eigen::Vector3d(beams[i]->p0->x);
                  particle_temp->v << Eigen::Vector3d(beams[i]->p0->v);
                  particle_temp->m = 100000.0;
                  particle_temp->fixed = false;
                  particle_temp->i = n;
                  n += 3;
                  beams[i] = createBeam(particle_temp, beams[i]->p1, 1e9, false);
                  particles.push_back(particle_temp);
               } 
               else {
                  particle_temp->x << Eigen::Vector3d(beams[i]->p1->x);
                  particle_temp->v << Eigen::Vector3d(beams[i]->p1->v);
                  particle_temp->m = 100000.0;
                  particle_temp->fixed = false;
                  particle_temp->i = n;
                  n += 3;
                  beams[i] = createBeam(beams[i]->p0, particle_temp, 1e9, false);
                  particles.push_back(particle_temp);
               }
            }
            else if (beams[i]->strain < -0.02){
               Particle *particle_temp = new Particle();
               if (rand() % 10 < 5) {
                  particle_temp->x << Eigen::Vector3d(beams[i]->p0->x);
                  particle_temp->v << Eigen::Vector3d(beams[i]->p0->v);
                  particle_temp->m = 100000.0;
                  particle_temp->fixed = false;
                  particle_temp->i = n;
                  n += 3;
                  beams[i] = createBeam(particle_temp, beams[i]->p1, 1e9, false);
                  particles.push_back(particle_temp);
               }
               else {
                  particle_temp->x << Eigen::Vector3d(beams[i]->p1->x);
                  particle_temp->v << Eigen::Vector3d(beams[i]->p1->v);
                  particle_temp->m = 100000.0;
                  particle_temp->fixed = false;
                  particle_temp->i = n;
                  n += 3;
                  beams[i] = createBeam(beams[i]->p0, particle_temp, 1e9, false);
                  particles.push_back(particle_temp);
               }
            }
         }
      }
   
   typedef Eigen::Triplet<double> T;
   std::vector<T> A_;
   Eigen::VectorXd sparse_b(n);

   for (int i = 0; i < particles.size(); i++) {
      if (!particles[i]->fixed && particles[i]->i >= 0) {

         A_.push_back(T(particles[i]->i, particles[i]->i, particles[i]->m + h * damp)); 
         A_.push_back(T(particles[i]->i + 1, particles[i]->i + 1, particles[i]->m + h * damp)); 
         A_.push_back(T(particles[i]->i + 2, particles[i]->i + 2, particles[i]->m + h * damp)); 
         
         sparse_b.segment<3>(particles[i]->i) =  particles[i]->m * particles[i]->v + h * particles[i]->m * grav;
      }
      
   }
   

   for (int i = 0; i < beams.size(); i++) {      
      int p0Idx, p1Idx;
      p0Idx = beams[i]->p0->i;
      p1Idx = beams[i]->p1->i;

      if (p0Idx >= 0 || p1Idx >= 0) {
         Eigen::Vector3d deltaX = beams[i]->p1->x - beams[i]->p0->x; 
         double l = deltaX.norm();
         double L = beams[i]->L;
         double Y = beams[i]->K;
         bool between = checkIfBetween(externPos, beams[i]->p0, beams[i]->p1);

         double a = 0, b = 0;

         if (beams[i]->isRoad && between) {
            a = 1 - (getDistance(externPos, beams[i]->p0) / l);
            b = 1 - (getDistance(externPos, beams[i]->p1) / l);
         }
         
         // Force
         Eigen::Vector3d force = Y * (l - L)/l * deltaX; 

         // Calculates Stiffness
         Eigen::Matrix3d Ks = (-Y / (l * l)) * (((((l - L) / l)) * ((double)(deltaX.transpose()  * deltaX)) * Eigen::Matrix3d::Identity()) +
                              (1 - (l - L) / l) * (deltaX * deltaX.transpose()));
         if (p0Idx >= 0) {
            for (int m = 0; m < 3; m++) {
               for (int n = 0; n < 3; n++) {
                  A_.push_back(T(p0Idx + m, p0Idx + n, -h*h*Ks(m, n)));
               }
            }
            sparse_b.segment<3>(p0Idx) += h*force;
            if (beams[i]->isRoad && between) {
               sparse_b.segment<3>(p0Idx) += a * externalForce;
            }
         }
         if (p1Idx >= 0) {
            for (int m = 0; m < 3; m++) {
               for (int n = 0; n < 3; n++) {
                  A_.push_back(T(p1Idx + m, p1Idx + n, -h*h*Ks(m, n)));
               }
            }
            sparse_b.segment<3>(p1Idx) -= h*force;
            if (beams[i]->isRoad && between) {
               sparse_b.segment<3>(p1Idx) += b * externalForce;
            }
         }
         if (p0Idx >= 0 && p1Idx >= 0) {
            for (int m = 0; m < 3; m++) {
               for (int n = 0; n < 3; n++) {
                  A_.push_back(T(p0Idx + m, p1Idx + n, -h*h*-Ks(m, n)));
                  A_.push_back(T(p1Idx + m, p0Idx + n, -h*h*-Ks(m, n)));
               }
            }
         }
      }
   }
   
   
   Eigen::SparseMatrix<double> sparse_A(sparse_b.size(), sparse_b.size());
   sparse_A.setFromTriplets(A_.begin(), A_.end());
   
   Eigen::SimplicialLDLT<Eigen::SparseMatrix<double> > solver;
   Eigen::VectorXd solution2 = solver.compute(sparse_A).solve(sparse_b);

   for (int i = 0; i < particles.size(); i++) {
      if (!particles[i]->fixed) {
         particles[i]->v = solution2.segment<3>(particles[i]->i);
         particles[i]->x += h * particles[i]->v;
      }
   }
	
	// Update position buffers
	update();	
}

void Bridge::draw(MatrixStack &MV, const Program &p) const
{
	// Draw mesh
	MV.pushMatrix();
	glUniformMatrix4fv(p.getUniform("MV"), 1, GL_FALSE, MV.topMatrix().data());
	
   int h_pos = p.getAttribute("vertPos");
	GLSL::enableVertexAttribArray(h_pos);
	glBindBuffer(GL_ARRAY_BUFFER, posBufID);
	glBufferData(GL_ARRAY_BUFFER, posBuf.size()*sizeof(float), &posBuf[0], GL_DYNAMIC_DRAW);
   
   glVertexAttribPointer(h_pos, 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);
   
   int h_color = p.getAttribute("vertColor");
	GLSL::enableVertexAttribArray(h_color);
	glBindBuffer(GL_ARRAY_BUFFER, colorBufID);
	glBufferData(GL_ARRAY_BUFFER, colorBuf.size()*sizeof(float), &colorBuf[0], GL_DYNAMIC_DRAW);
   
   glVertexAttribPointer(h_color, 3, GL_FLOAT, GL_FALSE, 0, (const void *)0);

	glLineWidth(3.0f);
   
   glDrawArrays(GL_LINES, 0, posBuf.size()/3);
	
   GLSL::disableVertexAttribArray(h_pos);
   GLSL::disableVertexAttribArray(h_color);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
   MV.popMatrix();	
}

#ifndef __L11__Scene__
#define __L11__Scene__

#include <stdio.h>
#include <vector>
#include <Eigen/Dense>

class Bridge;
class Particle;
class MatrixStack;
class Program;
class Shape;

class Scene
{
public:
	Scene();
	virtual ~Scene();
	
	void load();
	void init();
	void tare();
	void reset();
	void step(bool breakingBridge);
   void switchBridge();

	void draw(MatrixStack &MV, const Program &prog) const;
	
	double getTime() const { return t; }
	
   Eigen::Vector3d externalForce;
   Eigen::Vector3d externPos;
	
private:
	double t;
	double h;
	Eigen::Vector3d grav;
	double damp;
	
	Shape *sphereShape;
   
   int curBridge;
   Bridge *bridges[5];
	Bridge *bridge;
	std::vector<Particle *> spheres;
};

#endif /* defined(__L11__Scene__) */

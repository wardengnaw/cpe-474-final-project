#pragma once
#ifndef __BEAM_H__
#define __BEAM_H__
#include <stdio.h>

class Particle;

class Beam
{
   public:
      Beam(Particle *p0, Particle *p1);
      virtual ~Beam();

      Particle *p0;
      Particle *p1;
      double K;
      double L;
      double strain;
      bool isRoad;
};
#endif 
